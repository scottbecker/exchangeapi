﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Script.Serialization;
using ExchangeRateAPIModels.ExchangeRates;

namespace ExchangeRateAPIModels
{
    public class ExchangeRateHelper
    {
        public static async Task<BaseExchangeRateHandler.ResponseModel> GetExchangeRate(string baseCurrency, string targetCurrency)
        {
            ValidateInputs(baseCurrency, targetCurrency);
            
            return await BaseExchangeRateHandler.Get(baseCurrency, targetCurrency).HandleExchange();
        }

        private static List<string> supportedTypes = new List<string> { "AUD", "SEK", "USD", "GBP", "EUR" };
        public static void ValidateInputs(string baseCurrency, string targetCurrency)
        {
            var currencies = new List<string> { baseCurrency, targetCurrency };
            if (currencies.Any(x => !supportedTypes.Contains(x)))
            {
                throw new Exception($"Unsupported type. Currently supported types: {string.Join(", ", supportedTypes)}");
            }
            if (currencies.Any(x => string.IsNullOrEmpty(x)))
            {
                throw new Exception("Currencies are both required fields");
            }
            if (baseCurrency.ToLower() == targetCurrency.ToLower())
            {
                throw new Exception("Matching Currency Not Allowed");
            }
        }
    }
}
