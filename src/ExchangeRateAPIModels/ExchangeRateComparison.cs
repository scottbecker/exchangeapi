﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExchangeRateAPIModels
{
    public class ExchangeRateComparison
    {
        public string BaseCurrency { get; set; }
        public string TargetCurrency { get; set; }
        public decimal ExchangeRate { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}