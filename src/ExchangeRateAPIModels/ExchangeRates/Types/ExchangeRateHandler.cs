﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ExchangeRateAPIModels.ExchangeRates.Types
{
    public class ExchangeRateHandler : BaseExchangeRateHandler
    {
        public ExchangeRateHandler(string baseCurrency, string targetCurrency) : base(baseCurrency, targetCurrency)
        {

        }

        protected override double GetExchangeRateFromResponse(APIResponseModel responseModel)
        {
            return responseModel.rates[targetCurrency];
        }

        protected override Task<APIResponseModel> GetResponseData()
        {
            return GetResponseDataFromAPI(baseCurrency, targetCurrency);
        }
    }
}