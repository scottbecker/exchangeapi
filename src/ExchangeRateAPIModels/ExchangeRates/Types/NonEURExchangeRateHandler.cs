﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ExchangeRateAPIModels.ExchangeRates.Types
{

    //TODO: Remove when we upgrade to a different fixer plan
    public class NonEURExchangeRateHandler : BaseExchangeRateHandler
    {
        private string overwriteTargetCurrency { get; set; }
        public NonEURExchangeRateHandler(string baseCurrency, string targetCurrency) : base(baseCurrency, targetCurrency)
        {
            overwriteTargetCurrency = targetCurrency + "," + baseCurrency;
        }

        protected override double GetExchangeRateFromResponse(APIResponseModel responseModel)
        {
            var baseRateExchange = responseModel.rates[baseCurrency];
            var targetRateExchange = responseModel.rates[targetCurrency];
            return baseRateExchange / targetRateExchange;
        }

        protected override Task<APIResponseModel> GetResponseData()
        {
            return GetResponseDataFromAPI("EUR", overwriteTargetCurrency);
        }
    }

}