﻿using ExchangeRateAPIModels.ExchangeRates.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace ExchangeRateAPIModels.ExchangeRates
{
    public abstract class BaseExchangeRateHandler
    {

        protected string baseCurrency { get; set; }
        protected string targetCurrency { get; set; }
        public BaseExchangeRateHandler(string baseCurrency, string targetCurrency)
        {
            this.baseCurrency = baseCurrency;
            this.targetCurrency = targetCurrency;
        }
        public static BaseExchangeRateHandler Get(string baseCurrency, string targetCurrency)
        {
            if (baseCurrency != "EUR")
            {
                return new NonEURExchangeRateHandler(baseCurrency, targetCurrency);
            }
            return new ExchangeRateHandler(baseCurrency, targetCurrency);
        }



        public class ResponseModel
        {
            public string baseCurrency { get; set; }
            public string targetCurrency { get; set; }
            public double exchangeRate { get; set; }
            public string timestamp { get; set; }
        }
        protected class APIResponseModel
        {
            public bool success { get; set; }
            public int timestamp { get; set; }
            public string @base { get; set; }
            public Dictionary<string, double> rates { get; set; }
        }
        protected class APIFailModel
        {
            public bool success { get; set; }
            public APIErrorModel error { get; set; }
        }
        protected class APIErrorModel
        {
            public int code { get; set; }
            public string info { get; set; }
        }

        public async Task<ResponseModel> HandleExchange()
        {
            var responseModel =  await GetResponseData();
            return ProcessSuccessfulResponse(responseModel);
        }

        protected abstract Task<APIResponseModel> GetResponseData();

        protected async Task<APIResponseModel> GetResponseDataFromAPI(string baseCurrency, string targetCurrency)
        {
            var client = new HttpClient();
            var response = await client.GetAsync($"http://data.fixer.io/api/latest?access_key=49798b0b12e54240bd265a8abc6732d3&base={baseCurrency}&symbols={targetCurrency}");
            response.EnsureSuccessStatusCode();

            var responseBody = await response.Content.ReadAsStringAsync();
            var responseModel = new JavaScriptSerializer().Deserialize<APIResponseModel>(responseBody);
            if (!responseModel.success)
            {
                var failModel = new JavaScriptSerializer().Deserialize<APIFailModel>(responseBody);
                throw new Exception($"{failModel.error.code}: {failModel.error.info}");
            }
            return responseModel;
        }

        protected ResponseModel ProcessSuccessfulResponse(APIResponseModel responseModel)
        {
            return new ResponseModel()
            {
                baseCurrency = responseModel.@base,
                exchangeRate = GetExchangeRateFromResponse(responseModel),
                targetCurrency = targetCurrency,
                timestamp = UnixTimeStampToDateTime(responseModel.timestamp).ToString()
            };
        }

        protected static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
        protected abstract double GetExchangeRateFromResponse(APIResponseModel responseModel);
    }
}