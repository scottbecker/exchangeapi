﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExchangeRateAPIModels
{
    public class ExchangeRate
    {
        private string _symbol { get; set; }
        public string Symbol => _symbol;

        private double _val { get; set; }
        public double Value => _val;

        public ExchangeRate(string symbol, double value)
        {
            this._symbol = symbol;
            this._val = value;
        }
    }
}