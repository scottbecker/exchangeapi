﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ExchangeRateAPI.Controllers
{
    public class ExchangeController : ApiController
    {
        public class GetRequestModel
        {
            public string baseCurrency { get; set; }
            public string targetCurrency { get; set; }
        }
        
        //This should be in a DTO that we process and return
        public async Task<ExchangeRateAPIModels.ExchangeRates.BaseExchangeRateHandler.ResponseModel> Get([FromBody]GetRequestModel param)
        {
            return await ExchangeRateAPIModels.ExchangeRateHelper.GetExchangeRate(param.baseCurrency, param.targetCurrency);
        }
    }
}
